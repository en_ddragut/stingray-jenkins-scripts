/*
 *
 * Purpose : Stigray - add Git branch, commit and date in footer
 * Author  : Dan Dragut <dan.dragut@endava.com>
 *
 */
@Grab(group='org.apache.commons', module='commons-lang3', version='3.7')
import org.apache.commons.lang3.StringUtils;
import hudson.model.*;
import groovy.util.AntBuilder;

/*
 * Init...
 */
def envVars = System.getenv();

/*
 * Git branch name...
 */
def GIT_BRANCH = envVars["GIT_BRANCH"] ?: envVars["GIT_BRANCH"].split("/")[1] ;
if (StringUtils.isBlank(GIT_BRANCH)) {
   GIT_BRANCH = ("git rev-parse --abbrev-ref HEAD").execute().text;
}
// println GIT_BRANCH;

/*
 * Git last commit hash
 */
def GIT_COMMIT = ("git log --pretty=format:%h -n 1").execute().text;
// println GIT_COMMIT;
 
/*
 * Git last commit date
 */
def GIT_COMMIT_DATE = ("git log --pretty=format:%aD -n 1").execute().text;
// println GIT_COMMIT_DATE;

/*
 * Git changelog
 */
def GIT_CHANGELOG = ("git log --pretty=format:\"%h  %<(20)%an  %ai  %s\" -n 300").execute().text;
new File("web/web/changelog.txt").withWriter('UTF-8') { writer ->
    writer.write(GIT_CHANGELOG);
}

/*
 * foot.jsp - add text to footer
 */ 
def AntBuilder ant = new AntBuilder() ;
ant.replace(file  : "web\\WEB-INF\\jspf\\foot.jsp",
            token : "<div id=\"serverId\">", 
			value : "<div id=\"serverId\">"
			      + "<div style=\"float: right; vertical-align: middle; padding: 10px\"><a href=\"/stingray/web/changelog.txt\" target=\"changelog\" style=\"font: 9pt monospace; font-weight: bold; color: #ffffff;\">$GIT_BRANCH | $GIT_COMMIT | $GIT_COMMIT_DATE</a></div>");

/*
 * HALT (development only)
 */ 
// throw new Exception("DEVELOPMENT - STOP HERE")