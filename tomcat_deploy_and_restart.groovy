/*
 * Imports...
 */
@Grab(group='org.apache.commons', module='commons-lang3', version='3.5')
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
@Grab(group='commons-io', module='commons-io', version='2.5')
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import groovy.util.AntBuilder;

/*
 * Parameters...
 */
try {
  def build        = Thread.currentThread().executable;
}
catch (groovy.lang.MissingPropertyException e) {
  throw new RuntimeException("ERROR: Should configure script as \"Execute SYSTEM Groovy script\".");
}

def variables      = binding.variables;
def TOMCAT_SERVICE = variables.get('TOMCAT_SERVICE');
def WAR            = variables.get('WAR');

/*
 *
 */
println "";
println "################################";
println "#  TOMCAT : deploy + restart  #"
println "##############################"
println "TOMCAT_SERVICE = $TOMCAT_SERVICE";

/*
 * Check TOMCAT_SERVICE...
 */

// Get service command line...
def command = "sc qc \"${TOMCAT_SERVICE}\"";
def process = command.execute();
process.waitFor();
def output = process.text;
if (process.exitValue() > 0) {
    println output;
	return process.exitValue();
}

// Get executable path from command line...
def commandLine;
output.eachLine() { line ->
    if (line =~ /BINARY_PATH_NAME/) {
	    commandLine = StringUtils.split(line, ":", 2)[1];
		commandLine = StringUtils.trim(commandLine);
		return true;
	}
}

def executablePath;
if (StringUtils.startsWith(commandLine, "\"")) {
    executablePath = StringUtils.substringBetween(commandLine, "\"", "\"");
}
else {
    executablePath = StringUtils.substringBefore(commandLine, " ");
}
// DEBUG: println executablePath;

// Check it exists and get TOMCAT_HOME from it...
def file = FileUtils.getFile(executablePath);
if (StringUtils.isBlank(executablePath) || !file.exists() || !file.isFile()) {
    throw new FileNotFoundException("ERROR: Could not identify TOMCAT_HOME from ${TOMCAT_SERVICE} service definition.");
}
def TOMCAT_HOME = file.getParentFile().getParentFile().getAbsolutePath();
println "TOMCAT_HOME    = $TOMCAT_HOME";

// Check webapps...
file = FileUtils.getFile(TOMCAT_HOME, "webapps");
println file.getAbsolutePath() + " " + file.exists() + " " + file.isDirectory();
if (!file.exists() || !file.isDirectory()) {
    throw new FileNotFoundException("ERROR: Invalid TOMCAT_HOME? \"webapps\" missing.");
}

/*
 * Check WAR file exists...
 */
println "WAR            = $WAR";
println "";

file = FileUtils.getFile(WAR);
if (!file.exists()) {
  // Path relative to current workspace?
  file = FileUtils.getFile(build.workspace.toString(), WAR);
}
if (StringUtils.isBlank(WAR) || !file.exists() || !file.isFile()) { 
    throw new FileNotFoundException("File not found or not a file! Check WAR parameter.");
}
WAR = file.getAbsolutePath();

/*
 * Stop service...
 */
command = "sc stop ${TOMCAT_SERVICE}";
println "Stopping '${TOMCAT_SERVICE}' service..."
process = command.execute();
process.waitFor();

/*
 * Wait to stop..
 */
while (true) {
    sleep(2000);
	command = "sc query $TOMCAT_SERVICE";
	Log.debug(command);
	process = command.execute();
	process.waitFor();
    output = process.text;
	// DEBUG println output;
    if (process.exitValue() > 0) {
		return process.exitValue();
    }
    if (output.contains("STOPPED")) {
	   break;
	}
}

/*
 * Copy new WAR...
 */
def warName = FilenameUtils.getName(WAR);
def oldWAR  = FileUtils.getFile(TOMCAT_HOME, "webapps", warName);
def newWAR  = new File(WAR);

// Delete old...
if (oldWAR.exists()) {
    println "Deleting '$oldWAR'...";
    FileUtils.forceDelete(oldWAR);
	
	def oldWarDir = FileUtils.getFile(TOMCAT_HOME, "webapps", FilenameUtils.getBaseName(warName));
	println "Deleting '${oldWarDir}'...";
	FileUtils.forceDelete(oldWarDir);
}

// Copy new...
println "Copying  '$newWAR' to '$oldWAR'..."
FileUtils.copyFile(newWAR, oldWAR);

/*
 * Clean log files...
 */
def logsDir = FileUtils.getFile(TOMCAT_HOME, "logs");
println "Cleaning '${logsDir}'..."
for (File logFile : logsDir.listFiles()) {
    if (!logFile.isDirectory() && FilenameUtils.getExtension(logFile.getName()).equals("log")) {
        FileUtils.deleteQuietly(logFile);
    }
}

/*
 * Start service...
 */
println "Starting '$TOMCAT_SERVICE' service...";
process = "sc start $TOMCAT_SERVICE".execute();
process.waitFor();
// DEBUG println process.text;
if (process.exitValue() > 0) {
    return process.exitValue();
}

println "";
return 0;

/*
 *
 */
class Log {
  def DEBUG = true;
  
  public static void debug(String message) {
    println message;
  }
}