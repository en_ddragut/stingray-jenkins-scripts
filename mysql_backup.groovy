/*
 *
 * Purpose : Backup MySQL database
 * Author  : Dan Dragut <dan.dragut@endava.com>
 *
 */
import groovy.util.AntBuilder;
// @Grab(group='commons-io', module='commons-io', version='2.5')
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

/*
 * Variables...
 */
def variables   = binding.variables;
def SCHEMA      = variables.get('SCHEMA');
def USER        = variables.get('USER');
def PASSWORD    = variables.get('PASSWORD');
def LOCATION    = variables.get('LOCATION');
def KEEP_DAYS   = variables.get('KEEP_DAYS') ?: "14";		// Two weeks (default)

/*
 * Initial dump location...
 */
println "#################"
println "#   PREPARE    #"
println "###############"
 
def MYSQL_SECURE_FILE_PRIV = ("mysql $SCHEMA --user=$USER --password=$PASSWORD -N -B -e \"SELECT @@global.secure_file_priv\"").execute().text.readLines()[0];
println ""
println "MySQL secure_file_priv = $MYSQL_SECURE_FILE_PRIV"

def NOW               = new Date().format("yyyy-MM-dd'T'HH.mm.ss'Z'", TimeZone.getTimeZone("UTC"));
def DUMP_DIR_NAME     = SCHEMA + "_" + NOW;
def MYSQL_DB_DUMP_DIR = new File(MYSQL_SECURE_FILE_PRIV, DUMP_DIR_NAME);
MYSQL_DB_DUMP_DIR.mkdir();
println "MySQL DB dump dir      = " + MYSQL_DB_DUMP_DIR.getAbsolutePath();
println ""

/*
 * MySQL dump...
 */
println "#################"
println "#   DUMP       #"
println "###############"

def MYSQLDUMP_COMMAND = "mysqldump $SCHEMA --user=$USER --password=$PASSWORD" \
                        + " --tab=\"${MYSQL_DB_DUMP_DIR}\""                   \
					    + " --quote-names --dump-date --opt --single-transaction --triggers";
println MYSQLDUMP_COMMAND

def MYSQLDUMP_PROCESS = (MYSQLDUMP_COMMAND).execute();
def stdOut            = new StringBuffer();
def stdErr            = new StringBuffer();
MYSQLDUMP_PROCESS.consumeProcessOutput(stdOut, stdErr);
MYSQLDUMP_PROCESS.waitFor();

// ERROR!
if (MYSQLDUMP_PROCESS.exitValue() > 0) {
  println stdErr
  println stdOut
  return MYSQLDUMP_PROCESS.exitValue();
}
// SUCCESS!
else {
  println stdOut;
}

/*
 * Compress...
 */
println "#################"
println "#   COMPRESS   #"
println "###############"

def MYSQL_DB_DUMP_ZIP = new File(LOCATION, "${DUMP_DIR_NAME}.zip")
println "MYSQL_DB_DUMP_ZIP      = $MYSQL_DB_DUMP_ZIP";
new AntBuilder().zip(destFile : MYSQL_DB_DUMP_ZIP, \
                     baseDir  : MYSQL_DB_DUMP_DIR.getAbsolutePath());
					 
/*
 * Cleanup (remove dump files)
 */
println ""
println "#################"
println "#   CLEANUP    #"
println "###############"

println "Removing $MYSQL_DB_DUMP_DIR directory..."
MYSQL_DB_DUMP_DIR.deleteDir();

println ""
println "Trimming backups (KEEP_DAYS = ${KEEP_DAYS}):"
def REMOVE_OLDER_THAN = new Date().minus(KEEP_DAYS.toInteger()).getTime();
def counter           = 0;
for (File file : new File(LOCATION).listFiles()) {
	if (file.isFile()
		&& FilenameUtils.getExtension(file.getName()).equals("zip")
		&& file.lastModified() <= REMOVE_OLDER_THAN) {
		println "> ${file.absolutePath}"
		FileUtils.deleteQuietly(file);
		counter++;
	}
}
println "${counter} files deleted."
/*
 * The End
 */