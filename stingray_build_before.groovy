/*
 *
 * Purpose : AJG Stingray - before build actions:
 *           > allow to specify DB schema / user / password in dev.properties;
 *           > allow to specify Policy Wordings location;
 *           > display repo / branch / last commit in footer;
 *           > generate changelog (available from footer);
 *           > add support for FindBugs & PMD tasks
 * Author  : Dan Dragut <dan.dragut@endava.com>
 *
 */
import jenkins.*
@Grab(group='org.apache.commons', module='commons-lang3', version='3.7')
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.SystemUtils
import java.nio.charset.StandardCharsets

/*
 * Main
 */
// LOCAL DEV Mode
// def paramsMap = new HashMap()
// paramsMap.put("GIT_BRANCH", "xxxx/xyz")
// paramsMap.put("STINGRAY_DB_SCHEMA", "abcd")
// paramsMap.put("STINGRAY_DB_USER",   "b1234")
// paramsMap.put("STINGRAY_DB_PASS",   "c83473")
// paramsMap.put("STINGRAY_WORDINGS",  "D:/Jenkins/workspace/Stingray - Policy Wordings/docs")

// PROD Mode
def paramsMap = new HashMap()
paramsMap.putAll(System.getenv())
paramsMap.putAll(binding.variables)
args.each { arg ->
    arg.eachLine { _arg ->
        if (StringUtils.contains(_arg, "=")) {
            paramsMap.put(StringUtils.substringBefore(_arg, "="), StringUtils.substringAfter(_arg, "="))
        }
    }
}

// Action...
def beforeBuildActions = new StingrayBeforeBuildActions(paramsMap)
beforeBuildActions.generateGitFooter()
beforeBuildActions.changeDatabaseConnection()
beforeBuildActions.changePolicyWordings()
beforeBuildActions.prepareUnitTests()
beforeBuildActions.addCodeAnalisys()
beforeBuildActions.changeDeployXml()
return

/*
 *
 */
class StingrayBeforeBuildActions {
    /**
     * Constants
     */
    final static String BUILD_GRADLE          = "build.gradle"
    final static String DEPLOY_XML            = "deploy.xml"
    final static String CHANGELOG_TXT         = "web/web/changelog.txt"
    final static String DEV_PROPERTIES        = "src/conf/dev.properties"
    final static String FINDBUGS_EXCLUDES_XML = "findbugsExcludes.xml"

    /*
    * Members
    */
    private Map<String, String> mParameters

    /*
     * Constructor
     */
    StingrayBeforeBuildActions(Map<String, String> pParameters) {
        mParameters = pParameters
       
        // DEBUG
        // mParameters.each { name, value -> println "$name ... = ... $value" }

        println " "
        println "#######################"
        println "#  BUILD > Before... #"
        println "#####################"
    }
   
    /*
     * Generate Git information and inject into page footers
     */
    void generateGitFooter() {
        /*
         * Repository name...
         */
        String GIT_REPOSITORY_URL = ("git config --get remote.origin.url").execute().text
        String GIT_REPOSITORY     = StringUtils.substringBefore(StringUtils.substringAfterLast(GIT_REPOSITORY_URL, "/"), ".git")

        /*
         * Branch name (long format with last commit info)
         */
        def GIT_BRANCH_LONG
        ("git branch -vv").execute().text.eachLine { line ->
            if (StringUtils.startsWith(line, "* ")) {
                GIT_BRANCH_LONG = line
            }
            return
        }

        /*
         * Branch name...
         */
        def GIT_BRANCH = mParameters["GIT_BRANCH"]
        if (StringUtils.isNotBlank(GIT_BRANCH)) {
            GIT_BRANCH = GIT_BRANCH.split("/")[1]
        }
        else {
            GIT_BRANCH = ("git rev-parse --abbrev-ref HEAD").execute().text
        }
        // println GIT_BRANCH
        
        /*
         * Commit...
         */
        def GIT_COMMIT_HASH = ("git log --pretty=format:%h -n 1").execute().text
        // println GIT_COMMIT
        
        def GIT_COMMIT_DATE = ("git log --pretty=format:%aD -n 1").execute().text
        // println GIT_COMMIT_DATE
        
        /*
         * Changelog
         */
        println "> Generate '${CHANGELOG_TXT}'..."
        def GIT_CHANGELOG = ("git log --pretty=format:\"%h  %<(21)%an  %ai  %s\" -n 500").execute().text
        new File(CHANGELOG_TXT).withWriter('UTF-8') { writer ->
            writer.println(new Date())
            writer.println()
            writer.println(GIT_REPOSITORY_URL)
            writer.println(GIT_BRANCH_LONG)
            writer.println()
            writer.write(GIT_CHANGELOG)
        }
 
        /*
         * foot.jsp - add information to footer
         */ 
        AntBuilder ant = new AntBuilder()
        ant.replace(file  : "web\\WEB-INF\\jspf\\foot.jsp",
                    token : "<div id=\"serverId\">", 
                    value : "<div id=\"serverId\">"
                          + "<div style=\"float: right; vertical-align: middle; padding: 10px\">"
                          + "  <a href=\"/stingray/web/changelog.txt\" target=\"changelog\" style=\"font: 9pt monospace; font-weight: bold; color: #ffffff;\">$GIT_REPOSITORY/$GIT_BRANCH | $GIT_COMMIT_HASH | $GIT_COMMIT_DATE</a>"
                          + "</div>")
    }
    
    /*
     * Change DB schema / user / pass in dev.properties
     */
    void changeDatabaseConnection() {
        println "> Update '${DEV_PROPERTIES}'..."
        AntBuilder antBuilder     = new AntBuilder()
        
        /*
         * Schema name...
         */
        def DB_SCHEMA = mParameters.get("STINGRAY_DB_SCHEMA")
        if (StringUtils.isNotBlank(DB_SCHEMA)) {
            // DEBUG println "  STINGRAY_DB_SCHEMA = ${DB_SCHEMA}"
            antBuilder.replace(file  : DEV_PROPERTIES,
                               token : "hibernate.connection.url=jdbc:mysql://127.0.0.1:3306/stingray", 
                               value : "hibernate.connection.url=jdbc:mysql://127.0.0.1:3306/${DB_SCHEMA}")
        }
        
        /*
         * Username...
         */
        def DB_USER = mParameters.get("STINGRAY_DB_USER")
        if (StringUtils.isNotBlank(DB_USER)) {
            // DEBUG println "  STINGRAY_DB_USER   = ${DB_USER}"
            antBuilder.replace(file  : DEV_PROPERTIES,
                               token : "hibernate.connection.username=root", 
                               value : "hibernate.connection.username=${DB_USER}")
        }
        
        /*
         * Password...
         */
        def DB_PASS = mParameters.get("STINGRAY_DB_PASS")
        if (StringUtils.isNotBlank(DB_PASS)) {
            // DEBUG println "  STINGRAY_DB_PASS   = ${DB_PASS}"
            antBuilder.replace(file  : DEV_PROPERTIES,
                               token : "hibernate.connection.password=letmein", 
                               value : "hibernate.connection.password=${DB_PASS}")
        }
        
        /*
         * Print JDBC parameters
         */
         new File(DEV_PROPERTIES).eachLine { String line ->
             if (line.startsWith("hibernate.connection.url=")
              || line.startsWith("hibernate.connection.username=")
              || line.startsWith("hibernate.connection.password=")) {
                 println "  " + line
             }
         }
        
    }

    /**
     * Change Policy Wordings location (dev.properties)
     */
    void changePolicyWordings() {
        AntBuilder antBuilder = new AntBuilder()

        /*
         * Policy Wordings...
         */
        def STINGRAY_WORDINGS = mParameters.get("STINGRAY_WORDINGS")
        if (StringUtils.isNotBlank(STINGRAY_WORDINGS)) {
            // DEBUG println "WORDINGS  = ${STINGRAY_WORDINGS}"
            antBuilder.replace(file  : DEV_PROPERTIES,
                    token : "static.resources=/Users/greg/AJG/src/stingray/wordings/docs",
                    value : "static.resources=${STINGRAY_WORDINGS}")
        }
        
        /*
         * Print...
         */
         new File(DEV_PROPERTIES).eachLine { String line ->
             if (line.startsWith("static.resources=")) {
                 println "  " + line
             }
         }
    }

    /**
     * Prepare Unit tests
     */
    void prepareUnitTests() {
        println "> Fix tests..."
        
        /*
         * XmlFileProducerTest always fails on Windows
         */
        if (SystemUtils.IS_OS_WINDOWS) {
            new File("test/java/stingray/intersystem/transport/XmlFileProducerTest.java").delete()
        }

        /**
         * Full log stack traces
         */
        new File(BUILD_GRADLE).withWriterAppend { writer ->
            writer.println()
            writer.println()
            writer.println("test {")
            writer.println("    testLogging {")
            writer.println("        exceptionFormat = 'full'")
            writer.println("    }")
            writer.println("}")
            writer.println()
        }
    }

    /**
     * Add support for FindBugs & PMD
     */
    void addCodeAnalisys() {
        /*
         * FindBugs
         */
        new File(BUILD_GRADLE).withWriterAppend { writer ->
            writer.println("apply plugin: 'findbugs'")
            writer.println()
            writer.println("findbugs {")
            writer.println("    ignoreFailures      = true")
            writer.println("    sourceSets          = [ sourceSets.main] ")
            writer.println("    reportsDir          = file(\"\$project.buildDir/reports/findbugs\")")
            writer.println("    effort              = 'default'")
            writer.println("    excludeFilterConfig = resources.text.fromFile('findbugsExcludes.xml')")
            writer.println("}")
            writer.println()
            // Make findbugs output less verbose
            writer.println("afterEvaluate { Project project ->")
            writer.println("  project.tasks.withType(FindBugs) {")
            writer.println("    logging.captureStandardError(LogLevel.ERROR)")
            writer.println("    logging.captureStandardOutput(LogLevel.ERROR)")
            writer.println("  }")
            writer.println("}")
        }

        new File(FINDBUGS_EXCLUDES_XML).withWriter { writer ->
            writer.println("<FindBugsFilter>")
            writer.println("    <!-- Ignore all NM_SAME_SIMPLE_NAME_AS_SUPERCLASS -->")
            writer.println("    <Match>")
            writer.println("        <Bug pattern=\"NM_SAME_SIMPLE_NAME_AS_SUPERCLASS\" />")
            writer.println("    </Match>")
            writer.println("</FindBugsFilter>")
        }

        /*
         * PMD
         */
        new File(BUILD_GRADLE).withWriterAppend { writer ->
            writer.println("apply plugin: 'pmd'")
            writer.println()
            writer.println("pmd {")
            writer.println("    ignoreFailures = true")
            writer.println("}")
            writer.println()
            writer.println("ext {")
            writer.println("    repositories.jcenter()")
            writer.println("}")
        }
    }

    /**
     * Augment deploy.xml
     */
    void changeDeployXml() {
        println "> Update 'deploy.xml'..."

        /*
         * SRY-3922
         */

        // Read file...
        String deployXmlString = new File(DEPLOY_XML).getText(StandardCharsets.UTF_8.name())

        // Find...
        String targetDeployStringBefore = StringUtils.substringBetween(deployXmlString,
                "<target name=\"deploy\">",
                "</target>")

        // Replace...
        final String EXEC_ASADMIN_BEFORE = '<exec executable="asadmin">'
        final String EXEC_ASADMIN_AFTER  = '<exec executable="asadmin" logError="true" failonerror="true">'

        String targetDeployStringAfter  = StringUtils.replaceAll(targetDeployStringBefore, EXEC_ASADMIN_BEFORE, EXEC_ASADMIN_AFTER)
        deployXmlString = StringUtils.replace(deployXmlString, targetDeployStringBefore, targetDeployStringAfter)

        println "  ${EXEC_ASADMIN_AFTER}"

        // Write file...
        new File(DEPLOY_XML).write(deployXmlString, StandardCharsets.UTF_8.name())
    }
}